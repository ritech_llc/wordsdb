# WordsDB

`WordsDB` – библиотека, предназначенная для хранения сущностей (`entities`) и статистических данных о их встречаемости в документах коллекций. Данные, обрабатываемые этой библиотекой, используются в `ExTCL` для предсказания тематической принадлежности той или иной сущности, что необходимо для анализа и классификации документов.

Библиотека `WordsDB` имеет свой конфигурационный файл, находящийся по пути `etc/config/wordsdb.yml`. В данном файле настраиваются следующие параметры:

* `words_db_file` -- имя файла, в который ведется запись статистических данных;
* `words_db_lazy_writing` -- при установке данного параметра в значение `true`, библиотека WordsDB ведет запись в файл лишь при изменении каких-то данных, а не постоянно;
* `port` -- порт, через который ведется коммуникация с библиотекой `ExTCL`.

WordsDB содержит в себе такую информацию, как:

* `total_docs_count` - общее количество документов в коллекции
* `total_entities_count` - общее количество сущностей в коллекции
* `labels` - метки класса документа, которые присваиваются сущностям, встречающимся в данных документах
* `entities` - одно слово или несколько связанных синтаксическими связями слов.
* `entity_id` - уникальный идентификатор `entity`, полученный из нормальной формы слова или словосочетания (леммы). Для получения `entity_id` используется `libpyexbase`.
* `vocabulary` - словарь, хранящий в себе все сущности.

Для каждой сущности в `WordsDB` хранится информация о принадлежности к одному или нескольким классам: общее количество документов, где данная сущность встречается, количество употреблений данной сущности в коллекции документов, а также разбивка по меткам класса.

Далее перечислены основные интерфейсы WordsDB:

* Добавление статистики сущности. `JSON-RPC` вызов ``AddDocumentEntities`` с основным параметром `«entities»`, где `«entities»` – список словарей следующего вида:

```json
      {
            "id": "entity_id",
            "lemma": "lemma",
            "label": ["label1", "label2"],
            "count": 5
       }
```

* Получение общей статистики. `JSON-RPC` запрос `GetStat` возвращает следующее (перечислено основное):
 - общее кол-во сущностей (сколько раз всего встретились);
 - общее кол-во документов;
 - размер словаря (кол-во уникальных сущностей);
 - размер классов (кол-во меток классов);
 - словарь статистики по каждой метке класса отдельно:
     + общее кол-во сущностей (сколько раз всего встретилось) с этой меткой;
     + общее кол-во документов с этой меткой (`elq`);
* Получение статистики сущности. JSON-RPC запрос GetEntity (с основным параметром `entity_id`) возвращает следующее:
 - `entity_id`;
 - `lemma`;
 - общее кол-во сущностей (сколько раз всего втретилась);
 - общее кол-во документов, в которых встретилась (`nq`);


## Getting started

`WordsDB` поддерживает `multithreading` (многопоточное параллельное исполнение); 
WordsDB разработана с использованием языка `С++` и представляет собой отдельный модуль, работающий независимо от библиотеки `ExTCL`. 
Коммуникация с основным клиентом производится посредством `JSON-RPC` (`Remote Protocol Call`) запросов. 
Также `WordsDB` обладает такими возможностями, как многопоточная обработка поступающих запросов и поддержка нескольких меток для одного документа (`multilabel`).

### Install system packages:

```bash
sudo apt install build-essential libboost-system1.81-dev libboost-thread1.81-dev libboost-program-options1.81-dev libboost-test1.81-dev libboost-filesystem1.81-dev libboost-locale1.81-dev nlohmann-json3-dev
```


### Checkout/clone repository.


### Include git submodules

```bash
git submodule update --init
```

### Build and run

#### Build

```bash
mkdir -p build && cd build
cmake ../ -B ./
make -j4
```

#### Edit config

Set port and other params.
    
```bash
nano ../etc/config/wordsdb.yml
```

Parameters available:
* `words_db_file`: extcl.wdb -- путь к файлу, в который будет записан WordsDB при завершении работы программы
* `words_db_lazy_writing`: true -- не перезаписывать WordsDB, если не поступало новых сущностей после последнего сохранения
* `port`: 8550 -- номер порта


#### Run instance

```bash
./src/wordsdb -c ../etc/config/wordsdb.yml
```

#### В случае возникновения ошибок
Данные WordsDB сохраняются лишь при выключении (Graceful Stop, например, при помощи комбинации клавиш CTRL+C). 
Если по какой-то причине WordsDB перестал работать, тогда нужно его перезапустить. 
Затем при помощи метода получения статистики можно понять, какое количество документов добавлено в WordsDB. 
Если кол-во не сходится с требуемым кол-вом, тогда лучше рекомендуется полностью заново выполнить добавление документов. 


#### Try it with requests

##### AddDocumentEntities
```bash
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "AddDocumentEntities", "params": {"entities": [{"id": "e1", "lemma": "s1", "label": ["100"], "count": 123}]}}'
```
Output: 
```json
{"id":1,"jsonrpc":"2.0","result":true}
```

##### AddDocumentsEntities
```bash
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "AddDocumentsEntities", "params": {"docs_info": [{"entities": [{"id": "e1", "lemma": "s1", "label": ["100"], "count": 123},{"id": "e2", "lemma": "s2", "label": ["102"], "count": 321}]}]}}'
```
Output:
```json
{"id":1,"jsonrpc":"2.0","result":true}
```

##### AllEntities (10 items)
```bash
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "AllEntities", "params": {"limit": 10}}'
```

Output (formatted):
```json
{
  "id": 1,
  "jsonrpc": "2.0",
  "result": [
    {
      "id": "e1",
      "lemma": "s1",
      "stat": [
        [
          "100",
          [
            246,
            2
          ]
        ]
      ],
      "total_docs_count": 2,
      "total_entities_count": 246
    },
    {
      "id": "e2",
      "lemma": "s2",
      "stat": [
        [
          "102",
          [
            321,
            1
          ]
        ]
      ],
      "total_docs_count": 1,
      "total_entities_count": 321
    }
  ]
}
```

##### GetEntity
```bash
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "GetEntity", "params": {"id": "e1"}}'
```
Output:
```json
{"id":1,"jsonrpc":"2.0","result":[{"id":"e1","lemma":"s1","stat":[["100",[246,2]]],"total_docs_count":2,"total_entities_count":246}]}
```

##### GetEntities
```bash
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "GetEntities", "params": {"ids": ["e1", "e2"]}}'
```
Output (formatted):
```json
{
  "id": 1,
  "jsonrpc": "2.0",
  "result": [
    {
      "id": "e1",
      "lemma": "s1",
      "stat": [
        [
          "100",
          [
            246,
            2
          ]
        ]
      ],
      "total_docs_count": 2,
      "total_entities_count": 246
    },
    {
      "id": "e2",
      "lemma": "s2",
      "stat": [
        [
          "102",
          [
            321,
            1
          ]
        ]
      ],
      "total_docs_count": 1,
      "total_entities_count": 321
    }
  ]
}
```

##### Get stat
```
curl -X POST http://localhost:8550/jsonrpc -H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "id": 1, "method": "GetStat", "params": {"all": true}}'
```
Output (formatted):
```json
{
  "id": 1,
  "jsonrpc": "2.0",
  "result": {
    "labels": {
      "100": [
        246,
        2
      ],
      "102": [
        321,
        1
      ]
    },
    "labels_size": 2,
    "last_added_time": 1704737388,
    "total_docs_count": 2,
    "total_entities_count": 567,
    "vocabulary_size": 2
  }
}
```


#### Stop instance
`CTRL + C`

#### Play tests

```bash
ctest -j1 --no-compress-output -T Test
```
