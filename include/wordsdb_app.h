#pragma once


#include <fstream>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "jsonrpccxx/server.hpp"

#include "wordsdb_lib.h"


class WordsDBApp 
{   
  private:
    WordsDB wdb;
    WordsDBOptions opts;

  public:
    WordsDBApp()
    {
      init();
    }
    
    WordsDBApp(WordsDBOptions& opts_) 
    {
      opts = opts_;
      init();
    }
    
    void init(WordsDBOptions& opts_)
    {
      opts = opts_;
      init();
    }
    
    void init();
    void Stop();
    
    bool AddDocumentEntities(const doc_entities_t& doc_entities);
    bool AddDocumentsEntities(const docs_info_t& docs_info);
    wdb_entities_stat_t AllEntities(int limit = 10);
    wdb_entities_stat_t GetEntity(const entity_id_t& entity_id);
    wdb_entities_stat_t GetEntities(const entity_ids_t& entity_ids);    
    WordsDBStat GetWdbStat(bool all);
    
    ~WordsDBApp() {}
};


void add_wordsdb_app_jsonrpc_handlers(jsonrpccxx::JsonRpc2Server& rpcServer, WordsDBApp& app);
