
set(INCLUDE_DIRECTORIES
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_SOURCE_DIR}/thirdparty
    ${CMAKE_SOURCE_DIR}/thirdparty/json-rpc-cxx/include
    ${CMAKE_SOURCE_DIR}/thirdparty/json-rpc-cxx/examples
    ${CMAKE_SOURCE_DIR}/thirdparty/mini-yaml
    )
    
set(COMMON_LIBRARIES
    Boost::boost
    Boost::filesystem
    Boost::program_options
    Boost::serialization
    nlohmann_json::nlohmann_json
    )

    
# wordsdb_lib
add_library(wordsdb_lib
    wordsdb_lib.cpp
    types.cpp
    Yaml.cpp
    )

target_include_directories(wordsdb_lib PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )
    
target_link_libraries(wordsdb_lib PUBLIC
    ${COMMON_LIBRARIES}
    )


# wordsdb_app
add_library(wordsdb_app
    wordsdb_app.cpp
    )

target_include_directories(wordsdb_app PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )
    
target_link_libraries(wordsdb_app PUBLIC
    ${COMMON_LIBRARIES}
    wordsdb_lib
    )
    
        
# wordsdb
add_executable(wordsdb
    main.cpp
    )
    
target_include_directories(wordsdb PUBLIC 
    ${INCLUDE_DIRECTORIES}
    )
    
target_link_libraries(wordsdb PUBLIC
    ${COMMON_LIBRARIES}
    wordsdb_app
    )

install(TARGETS wordsdb
        RUNTIME DESTINATION bin)
