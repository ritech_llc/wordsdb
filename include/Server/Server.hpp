#pragma once

#include <string>
#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <utility>
#include <algorithm>
#include <sstream>
#include <sys/un.h>
#include <thread>
#include <vector>

#define BUF_SIZE 1024

#ifndef BYTE
  #define BYTE unsigned char
#endif

template<class Handler>
class Server{
private:
    int port;
    int connection_limit;
    int listener=0;
public:
    bool isActive;

    Server(int port, int connection_limit):port(port),connection_limit(connection_limit){
        std::cout << "Creating server instance" << std::endl;
    }

    ~Server(){
        std::cout << "Destructing server instance" << std::endl;
        if (this->listener != 0)
            this->Stop();
    }

    void Start()
    {
        std::cout << "Starting server" << std::endl;

        this->listener = socket(AF_INET, SOCK_STREAM, 0);
        if (listener < 0)
        {
            std::cerr << "Error while socket initialization" << std::endl;
            return;
        }

        std::cout << "Socket successfully initialized" << std::endl;

        struct sockaddr_in addr;
        std::memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = htons(this->port);

        if (bind(this->listener, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)))
        {
            std::cerr << "Error on binding stream socket" << std::endl;
            return;
        }

        std::cout << "Socket was successfully binded with port " << this->port << std::endl;

        listen(this->listener, this->connection_limit);
        isActive = true;

        for (;;)
        {
            auto msgsock = accept(this->listener, 0, 0);
            if (msgsock < 0)
            {
                std::cerr << "Error on accept" << std::endl;
                if (!isActive)
                    break;
            }
            else
            {
                std::thread T([](int client)
                              {
                                  int len;
                                  std::vector<BYTE> data;
                                  BYTE buf[BUF_SIZE];

                                  bzero(buf, sizeof(buf));
                                //Read data
                                  while ((len = recv(client, buf, BUF_SIZE, MSG_NOSIGNAL)) > 0)
                                  {
                                      data.insert(data.end(), buf, buf + len);
                                      if (buf[len - 1] == '\0')
                                          break;
                                      bzero(buf, sizeof(buf));
                                  }
                                //Handle
                                  if (data.size() > 0)
                                  {
                                      Handler H;
                                      data = H.handle(data);

                                      // Send response
                                      if (data.size() > 0)
                                      {
                                          send(client, data.data(), data.size(), MSG_NOSIGNAL);
                                      }
                                  }

                                  close(client);
                              },
                              msgsock);
                T.detach();
            }
        }
        std::cout << "Server thread was stopped" << std::endl;
    }

    void Stop(){
        isActive = false;
        std::cout << "Closing socket" << std::endl;
        shutdown(this->listener, SHUT_RDWR);
        close(this->listener);
    }

};
