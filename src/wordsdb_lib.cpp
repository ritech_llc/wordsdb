
#include <cassert>

#include "wordsdb_lib.h"

#include <boost/filesystem.hpp>
#include "yaml/Yaml.hpp"

void WordsDBOptions::loadConfigFile()
{
    if (boost::filesystem::is_regular_file(config_file))
    {
        Yaml::Node root;
        Yaml::Parse(root, config_file.c_str());
        words_db_file = root["words_db_file"].As<std::string>();
        words_db_lazy_writing = root["words_db_lazy_writing"].As<bool>();
        port = root["port"].As<int>();
        
        if (boost::filesystem::portable_file_name(words_db_file))
            return;
        else
            throw std::runtime_error("incorrect words_db_file: " + words_db_file);
    }
    else
        throw std::runtime_error("config file does not exist: " + config_file);
}


label_t WordsDB::addLabel(const std::string& str_label, entity_count_t count, doc_count_t doc_count)
{
    label_t label;
    str_label_to_label_t::iterator it;
    it = str_label_to_label.find(str_label);
    if (it == str_label_to_label.end())
    {
        // Hope, that labels count does not bigger than max value of label_t
        labels_vault.push_back(str_label);
        label = labels_vault.size() - 1;
        str_label_to_label[str_label] = label;
        labels[label] = std::make_pair(count, doc_count);
    }
    else
    {
        label = it->second;
        labels[label].first += count;
        labels[label].second += doc_count;
    }

    return label;
}


str_label_t WordsDB::getLabel(label_t label)
{
    return labels_vault[label];
}


void WordsDB::addEntity(const entity_id_t& entity_id, 
                        const entity_lemma_t& lemma, 
                        const client_label_t& client_labels, 
                        entity_count_t count,
                        doc_count_t doc_count,
                        std::unordered_map<str_label_t, label_t> doc_labels_map
                       )
{
    words_db_entities_t::iterator entity_it;
    entity_it = entities.find(entity_id);
    if (entity_it == entities.end())
    {
        entities_vault.push_back(lemma); 
        vault_entity_id_t str_key = entities_vault.size() - 1; 
        entity_to_vault[entity_id] = str_key;
        
        entities[entity_id][0] = std::make_pair(count, doc_count);

        for (const str_label_t& str_label: client_labels)
        {
            label_t label = doc_labels_map[str_label];
            entities[entity_id][label] = std::make_pair(count, doc_count);
        }
    }
    else
    {
        entities[entity_id][0].first += count;
        entities[entity_id][0].second += doc_count;
        
        for (const str_label_t& str_label: client_labels)
        {
            label_t label = doc_labels_map[str_label];
            label_counters_t::iterator labels_it;
            labels_it = entity_it->second.find(label);
            if (labels_it == entity_it->second.end())
            {
                entities[entity_id][label] = std::make_pair(count, doc_count);
            }
            else
            {
                entities[entity_id][label].first += count;
                entities[entity_id][label].second += doc_count;
            }
        }
    }
    
    updateLastAddedTime();
}

bool WordsDB::addDocumentEntities(const doc_entities_t& doc_entities)
{
    std::unique_lock lock(mutex_);
    
    std::unordered_map<str_label_t, label_t> doc_labels_map;
    
    total_docs_count += 1;
    
    // 0 is reserved for total labal counters
    label_t label = addLabel(total_counter_name, 0, 1);
    assert(label == 0);
    doc_labels_map[total_counter_name] = label;
    
    for (auto& doc_entity: doc_entities)
    {
        total_entities_count += doc_entity.count;
        
        label_t label = addLabel(total_counter_name, doc_entity.count, 0);
        assert(label == 0);
        
        for (const str_label_t& str_label: doc_entity.label)
        {
            std::unordered_map<str_label_t, label_t>::iterator it;
            it = doc_labels_map.find(str_label);
            if (it == doc_labels_map.end())
            {
                label_t label = addLabel(str_label, doc_entity.count, 1);
                doc_labels_map[str_label] = label;
            }
            else
            {
                label_t label = addLabel(str_label, doc_entity.count, 0);
            }
        }
    }
    
    for (auto& doc_entity: doc_entities)
        addEntity(doc_entity.id, doc_entity.lemma, doc_entity.label, doc_entity.count, 1, doc_labels_map);   
    
    return true;
}

WordsDBEntityStat WordsDB::getEntity(const entity_id_t& entity_id)
{
    std::shared_lock lock(mutex_);
    
    WordsDBEntityStat res;
    words_db_entities_t::iterator entity_it;
    entity_it = entities.find(entity_id);
    if (entity_it != entities.end())
    {
        res.id = entity_id;
        std::unordered_map<label_t,std::pair<total_entity_count_t,total_doc_count_t>>::iterator labels_it; 
        for (labels_it=entity_it->second.begin(); labels_it!=entity_it->second.end(); ++labels_it)
        {
            label_t label;
            total_entity_count_t total_entity_count;
            total_doc_count_t total_doc_count;
            label = labels_it->first;
            if (label == 0)
            {
                res.total_entities_count = labels_it->second.first;
                res.total_docs_count = labels_it->second.second;
            }
            else
            {
                total_entity_count = labels_it->second.first;
                total_doc_count = labels_it->second.second;
                str_label_t str_label = getLabel(label);
                res.stat.push_back(std::make_pair(str_label,std::make_pair(total_entity_count,total_doc_count)));
            }
            
            entity_to_vault_t::iterator vault_it;
            vault_it = entity_to_vault.find(entity_id);
            res.lemma = entities_vault[vault_it->second];
        }
    }
    std::sort(res.stat.begin(), res.stat.end(), LabelsComparatorByCount());
    return res;
}


wdb_entities_stat_t WordsDB::allEntities(int limit)
{
    uint cnt = 0;
    wdb_entities_stat_t res;
    
    if (limit < 0)
        limit = entities_vault.size();
    
    std::shared_lock lock(mutex_);
    
    std::unordered_map<vault_entity_id_t,entity_id_t> vault_to_entity;
    for (const auto& item: entity_to_vault)
        vault_to_entity[item.second] = item.first;
    
    lock.unlock();

    for (vault_entity_id_t i=0; i<entities_vault.size() && cnt < limit; i++)
    {
        WordsDBEntityStat wdb_entity_stat = getEntity(vault_to_entity[i]);
        if (wdb_entity_stat.id.size()==0)
            continue;
        
        res.push_back(wdb_entity_stat);
        cnt++;
    }
    return res;
}


void WordsDB::updateLastAddedTime()
{
    last_added_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
}


std::time_t WordsDB::getLastAddedTime()
{
    return last_added_time;
}
