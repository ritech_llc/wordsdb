#pragma once


#include <chrono>
#include <ctime>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <mutex>
#include <shared_mutex>

#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/vector.hpp>

#include "types.h"


struct WordsDBOptions 
{
    std::string config_file;
    std::string words_db_file;
    bool words_db_lazy_writing;
    int port;
    
    WordsDBOptions(){ port = 8550; }
    
    WordsDBOptions(std::string& config_file_): 
        config_file(config_file_)
    {
        loadConfigFile();
    };
    
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & config_file;
        ar & words_db_file;
        ar & words_db_lazy_writing;
    }
    
    void loadConfigFile();
};


class WordsDB
{
  private:
      
    WordsDBOptions opts;
    
    total_doc_count_t total_docs_count;
    total_entity_count_t total_entities_count;
    
    total_entity_count_t vocabulary_size;
    unsigned int labels_size;
    
    std::time_t last_added_time;
    
    label_counters_t labels;
    str_label_to_label_t str_label_to_label;
    labels_vault_t labels_vault;
    
    words_db_entities_t entities;
    entity_to_vault_t entity_to_vault;
    entities_vault_t entities_vault;
    
    mutable std::shared_mutex mutex_;
    
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        std::unique_lock lock(mutex_);
        
        ar & opts;
        ar & total_docs_count;
        ar & total_entities_count;
        ar & last_added_time;
        ar & labels;
        ar & str_label_to_label;
        ar & labels_vault;
        ar & entities;
        ar & entity_to_vault;
        ar & entities_vault;
    }

    
  public:
      
    WordsDB()
    {
        init();
    }

    WordsDB(WordsDBOptions& opts_)
    {
        init(opts_);
    }
    
    void init(WordsDBOptions& opts_)
    {
        opts = opts_;
        init();
    }
    
    void init()
    {
        total_docs_count = 0;
        total_entities_count = 0;
        entities = words_db_entities_t();
        entity_to_vault = entity_to_vault_t();
        entities_vault = entities_vault_t();
        updateLastAddedTime();
    }
    
    void updateLastAddedTime();
    std::time_t getLastAddedTime();
    
    WordsDB(const WordsDB&) = delete;
    WordsDB& operator=(const WordsDB&) = delete;
    
    label_t addLabel(const std::string& str_label, entity_count_t count, doc_count_t doc_count);
    str_label_t getLabel(label_t label);

    void addEntity(const entity_id_t& entity_id, 
                   const entity_lemma_t& lemma, 
                   const client_label_t& labels, 
                   entity_count_t count, 
                   doc_count_t doc_count, 
                   std::unordered_map<str_label_t, label_t> doc_labels_map
                   );
    
    
    bool addDocumentEntities(const doc_entities_t& doc_entities);

    WordsDBEntityStat getEntity(const entity_id_t& entity_id);

    wdb_entities_stat_t allEntities(int limit = 10);
    
    inline total_doc_count_t getTotalDocsCount()
    {
        return total_docs_count;
    }
    
    inline total_entity_count_t getTotalEntitesCount()
    {
        return total_entities_count;
    }
    
    inline label_counters_t& getLabelsCounters()
    {
        std::shared_lock lock(mutex_);
        return labels;
    }    
    
    inline str_label_counters_t getStrLabelsCounters()
    {
        std::shared_lock lock(mutex_);
        str_label_counters_t str_label_counters;
        for (auto& label: labels)
        {
            if (label.first == 0)
                continue;

            str_label_counters[labels_vault[label.first]] = label.second;
        }
        return str_label_counters;
    }    
    
    
    inline total_entity_count_t getVocabularySize()
    {
        std::shared_lock lock(mutex_);
        vocabulary_size = entities.size();
        return vocabulary_size;
    }
    
    inline unsigned int getLabelsSize()
    {
        std::shared_lock lock(mutex_);
        labels_size = labels.size() - 1; // keep in mind, that one is reserved for TOTAL
        if (labels_size < 0)
            return 0;
        return labels_size;
    } 

    ~WordsDB() {}
};


class LabelsComparator
{
  public:
    virtual bool operator() (const entity_labels_stat_t& s1, const entity_labels_stat_t& s2) = 0;
};


class LabelsComparatorByLabels: LabelsComparator
{
  public:
    inline bool operator() (const entity_labels_stat_t& s1, const entity_labels_stat_t& s2) override
    {
        return s1.first > s2.first;
    }
};


class LabelsComparatorByCount: LabelsComparator
{
  public:
    inline bool operator() (const entity_labels_stat_t& s1, const entity_labels_stat_t& s2) override
    {
        return s1.second.first > s2.second.first;
    }
};


class LabelsComparatorByDocs: LabelsComparator
{
  public:
    inline bool operator() (const entity_labels_stat_t& s1, const entity_labels_stat_t& s2) override
    {
        return s1.second.second > s2.second.second;
    }
};
