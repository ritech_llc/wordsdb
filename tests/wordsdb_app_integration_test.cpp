#define BOOST_TEST_MODULE wordsdb_app_integration_test
#include <boost/test/unit_test.hpp>

#include <jsonrpccxx/client.hpp>
#include <jsonrpccxx/server.hpp>

#include "inmemoryconnector.hpp"
#include "cpphttplibconnector.hpp"

#include "wordsdb_app.h"

namespace bunt = boost::unit_test;

using namespace jsonrpccxx;

// https://github.com/jsonrpcx/json-rpc-cxx/blob/master/test/integrationtest.hpp#L7
struct WordsDBAppIntegrationFixture
{
    WordsDBAppIntegrationFixture(): 
        rpcServer()
    {
        WordsDBOptions opts = WordsDBOptions();
        app.init(opts);   
        add_wordsdb_app_jsonrpc_handlers(rpcServer, app);
    }
    
    WordsDBApp app;
    JsonRpc2Server rpcServer;
    
    void do_integration_test_stuff(IClientConnector &clientConnector)
    {
        JsonRpcClient client(clientConnector, version::v2);
        
        doc_entities_t doc_entities;
        std::vector<std::string> labels1({"1", "first"});
        std::vector<std::string> labels2({"2"});
        std::vector<std::string> labels3({"3"});
        std::vector<std::string> labels4({"4"});
        std::vector<std::string> labels5({"5", "V", "fifth"});
        std::vector<std::string> labels6({"6", "sixth"});
        std::vector<std::string> labels7({"7"});
        doc_entities.push_back(DocEnitiyInfo("e1", "str1", labels1, 1));
        doc_entities.push_back(DocEnitiyInfo("e2", "str2", labels2, 1));
        doc_entities.push_back(DocEnitiyInfo("e3", "str3", labels3, 2));
        doc_entities.push_back(DocEnitiyInfo("e4", "str4", labels4, 3));
        doc_entities.push_back(DocEnitiyInfo("e5", "str5", labels5, 5));
        doc_entities.push_back(DocEnitiyInfo("e6", "str6", labels6, 8));
        doc_entities.push_back(DocEnitiyInfo("e7", "str7", labels7, 13));
        
        client.CallMethod<bool>(1, "AddDocumentEntities", {doc_entities});
        
        wdb_entities_stat_t wdb_entities;
        wdb_entities = client.CallMethod<wdb_entities_stat_t>(1, "AllEntities", {1});
        BOOST_TEST(wdb_entities.size() == 1);
        BOOST_TEST(wdb_entities[0].id == "e1");
        BOOST_TEST(wdb_entities[0].lemma == "str1");
        BOOST_TEST(wdb_entities[0].stat.size() == 2); // labels count
        auto first_label = wdb_entities[0].stat[0].first;
        auto second_label = wdb_entities[0].stat[1].first;
        BOOST_TEST(((first_label == "1" and second_label == "first") || (first_label == "first" and second_label == "1"))); // label
        BOOST_TEST(wdb_entities[0].stat[0].second.first == 1); // total entities count
        BOOST_TEST(wdb_entities[0].stat[0].second.second == 1); // total docs count
        
        wdb_entities = client.CallMethod<wdb_entities_stat_t>(1, "AllEntities", {-1}); // get all entities
        BOOST_TEST(wdb_entities.size() == 7);
        BOOST_TEST(wdb_entities[6].id == "e7");
        BOOST_TEST(wdb_entities[6].lemma == "str7");
        BOOST_TEST(wdb_entities[6].stat.size() == 1); // labels count
        BOOST_TEST(wdb_entities[6].stat[0].first == "7"); // label
        BOOST_TEST(wdb_entities[6].stat[0].second.first == 13); // total entities count
        BOOST_TEST(wdb_entities[6].stat[0].second.second == 1); // total docs count    

        for (const auto& entity: doc_entities)
        {
            WordsDBEntityStat got_entity;
            got_entity = client.CallMethod<std::vector<WordsDBEntityStat>>(1, "GetEntity", {entity.id})[0];
            BOOST_TEST(got_entity.id == entity.id);
            BOOST_TEST(got_entity.lemma == entity.lemma);
            BOOST_TEST(got_entity.stat.size() == entity.label.size()); // labels count
            bool label_presented = std::find(entity.label.begin(), entity.label.end(), got_entity.stat[0].first)!=entity.label.end();
            BOOST_TEST(label_presented);
            BOOST_TEST(got_entity.stat[0].second.first == entity.count); // total entities count
            BOOST_TEST(got_entity.stat[0].second.second == 1); // total docs count
        }
    }
};


BOOST_FIXTURE_TEST_CASE(wordsdb_app_integration_inmemory_test, WordsDBAppIntegrationFixture)
{
    InMemoryConnector inMemoryConnector(rpcServer);
    do_integration_test_stuff(inMemoryConnector);
}


BOOST_FIXTURE_TEST_CASE(wordsdb_app_integration_http_test, WordsDBAppIntegrationFixture)
{
    CppHttpLibServerConnector httpServer(rpcServer, 8551);
    httpServer.StartListening();
    CppHttpLibClientConnector httpConnector("localhost", 8551);
    do_integration_test_stuff(httpConnector);
}
