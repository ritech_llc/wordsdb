#pragma once

#include <thread>
#include <vector>

class ThreadPool{
private:
    ThreadPool();
    ~ThreadPool();
    ThreadPool(ThreadPool const&) = delete;
    ThreadPool& operator= (ThreadPool const&) = delete;

    std::vector<std::thread> _workers;

public:
    static ThreadPool& Pool();
    void startTask(void (*f)());
    void wait();
};
