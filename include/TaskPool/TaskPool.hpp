#pragma once

#include <mutex>
#include <condition_variable>
#include <queue>
#include <iostream>
#include "Semaphore.hpp"


template <class Task>
class TaskPool{
private:
    TaskPool():_active(true){
    }
    ~TaskPool(){
        for (auto &i:_queue){
            while (i.second.size()>0){
                auto j = i.second.front();
                i.second.pop();
                delete j;
            }
        }
    }
    TaskPool(TaskPool const &) = delete;
    TaskPool& operator= (TaskPool const &) = delete;

    //Queue of tasks
    std::mutex  _vectorLock;
    std::condition_variable queue_check;

    bool _active;
    std::unordered_map<long, std::queue<Task*>> _queue;
    typename std::unordered_map<long, std::queue<Task*>>::iterator _iterator;
    Semaphore _semaphore;
public:
    static TaskPool& Pool(){
        static TaskPool<Task> P;
        return P;
    }
    void pushTask(Task* T, long Id = 0){
        std::lock_guard<std::mutex> Guard(_vectorLock);
        if (_queue.find(Id) == _queue.end()){
            _queue[Id] = std::queue<Task*>();
        }
        _queue[Id].push(T);
        _semaphore.add();
        _iterator = _queue.begin();
    }
    Task* popTask(){
        _semaphore.enter();
        if (!_active) return 0;
        std::lock_guard<std::mutex> Guard(_vectorLock);
        Task* T = (*_iterator).second.front();
        (*_iterator).second.pop();
        if ((*_iterator).second.size() == 0){
            _queue.erase(_iterator);
            _iterator = _queue.begin();
        }
        else
        {
            _iterator++;
            if (_iterator == _queue.end())
                _iterator = _queue.begin();
        }
        return T;
    }
    void stopPool(){
        std::cout << "Stop task pool" << std::endl;
        _active = false;
        _semaphore.unleash();
    }
    bool isActive() const{
        return _active;
    }
};
