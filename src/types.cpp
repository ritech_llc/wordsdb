
#include "types.h"

#include <string>
#include <nlohmann/json.hpp>


void to_json(nlohmann::json &j, const DocEnitiyInfo& doc_entity)
{
    j["id"] = doc_entity.id;
    j["lemma"] = doc_entity.lemma;
    j["label"] = doc_entity.label;
    j["count"] = doc_entity.count;
}

void from_json(const nlohmann::json &j, DocEnitiyInfo& doc_entity)
{
    j.at("id").get_to(doc_entity.id);
    j.at("lemma").get_to(doc_entity.lemma);
    j.at("label").get_to(doc_entity.label);
    j.at("count").get_to(doc_entity.count);
}

void to_json(nlohmann::json &j, const doc_entities_t& doc_entities) 
{
    for (auto& doc_entity: doc_entities)
        j.push_back(doc_entity);
}

void from_json(const nlohmann::json &j, doc_entities_t& doc_entities) 
{
    for (auto& doc_entity: j)
    {
        
        DocEnitiyInfo d;
        doc_entity.at("id").get_to(d.id);
        doc_entity.at("lemma").get_to(d.lemma);
        doc_entity.at("label").get_to(d.label);
        doc_entity.at("count").get_to(d.count);
        doc_entities.push_back(d);
    }
}

void to_json(nlohmann::json &j, const DocInfo& doc_info)
{
    j["id"] = doc_info.id;
    j["entities"] = doc_info.entities;
}

void from_json(const nlohmann::json &j, DocInfo& doc_info)
{
    j.at("id").get_to(doc_info.id);
    j.at("entities").get_to(doc_info.entities);
}

void to_json(nlohmann::json &j, const docs_info_t& docs_info)
{
    for (auto& doc_info: docs_info)
        j.push_back(doc_info);
}

void from_json(const nlohmann::json &j, docs_info_t& docs_info)
{
    for (auto& doc_info: j)
    {
        DocInfo d;
        doc_info.at("entities").get_to(d.entities);
        docs_info.push_back(d);
    }
}

void to_json(nlohmann::json &j, const EntityInfo& entity)
{
    j["id"] = entity.id;
    j["label"] = entity.label;
    j["total_count"] = entity.total_count;
    j["total_docs_count"] = entity.total_docs_count;
}

void from_json(const nlohmann::json &j, EntityInfo& entity)
{
    j.at("id").get_to(entity.id);
    j.at("label").get_to(entity.label);
    j.at("total_count").get_to(entity.total_count);
    j.at("total_docs_count").get_to(entity.total_docs_count);
}

void to_json(nlohmann::json &j, const entity_stat_t& entity_stat)
{
    for (auto& label_stat: entity_stat)
        j.push_back({{label_stat.first, {label_stat.second.first, label_stat.second.second}}});
}

void from_json(const nlohmann::json &j, entity_stat_t& entity_stat)
{
    for (auto& label_stat: j)
        entity_stat.push_back(std::make_pair(label_stat[0],std::make_pair(label_stat[1][0], label_stat[1][1])));
}

void to_json(nlohmann::json &j, const wdb_entities_stat_t& wdb_entities)
{
    for (auto& wdb_entity: wdb_entities)
        j.push_back({
            {"id", wdb_entity.id},
            {"lemma", wdb_entity.lemma},
            {"total_docs_count", wdb_entity.total_docs_count},
            {"total_entities_count", wdb_entity.total_entities_count},
            {"stat", wdb_entity.stat}
        });
}

void from_json(const nlohmann::json &j, wdb_entities_stat_t& wdb_entities)
{
    for (auto& wdb_entity: j)
    {
        WordsDBEntityStat wdb_entity_info;
        wdb_entity.at("id").get_to(wdb_entity_info.id);
        wdb_entity.at("lemma").get_to(wdb_entity_info.lemma);
        wdb_entity.at("total_docs_count").get_to(wdb_entity_info.total_docs_count);
        wdb_entity.at("total_entities_count").get_to(wdb_entity_info.total_entities_count);
        wdb_entity.at("stat").get_to(wdb_entity_info.stat);
        wdb_entities.push_back(wdb_entity_info);
    }
}

void to_json(nlohmann::json &j, const WordsDBStat& wdb_stat)
{
    j["total_docs_count"] = wdb_stat.total_docs_count;
    j["total_entities_count"] = wdb_stat.total_entities_count;
    j["vocabulary_size"] = wdb_stat.vocabulary_size;
    j["labels_size"] = wdb_stat.labels_size;
    j["last_added_time"] = wdb_stat.last_added_time;
    j["labels"] = wdb_stat.labels;
}

void from_json(const nlohmann::json &j, WordsDBStat& wdb_stat)
{
    j.at("total_docs_count").get_to(wdb_stat.total_docs_count);
    j.at("total_entities_count").get_to(wdb_stat.total_entities_count);
    j.at("vocabulary_size").get_to(wdb_stat.vocabulary_size);
    j.at("labels_size").get_to(wdb_stat.labels_size);
    j.at("last_added_time").get_to(wdb_stat.last_added_time);
    j.at("labels").get_to(wdb_stat.labels);
}
