#include "ThreadPool.hpp"

ThreadPool::ThreadPool(){

}

ThreadPool::~ThreadPool(){ 

}

ThreadPool& ThreadPool::Pool(){
    static ThreadPool Pool;
    return Pool;
}

void ThreadPool::startTask(void (*f)()){
    _workers.push_back(std::thread(f));
}

void ThreadPool::wait(){
    for (auto &i:_workers){
        i.join();
    }
}
