#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>


typedef std::string entity_id_t;
typedef std::vector<entity_id_t> entity_ids_t;
typedef std::string entity_lemma_t;
typedef std::string str_label_t;
typedef std::vector<str_label_t> client_label_t;
typedef unsigned char label_t;
typedef std::vector<label_t> doc_entity_label_t;
typedef unsigned long int entity_count_t;
typedef unsigned long int doc_count_t;
typedef unsigned long long int total_entity_count_t;
typedef unsigned long long int total_doc_count_t;
typedef std::string doc_id_t;

const std::string total_counter_name = "#TOTAL#";

struct DocEnitiyInfo
{
    entity_id_t id;
    entity_lemma_t lemma;
    client_label_t label;
    entity_count_t count;
    
    DocEnitiyInfo(){}
    DocEnitiyInfo(const entity_id_t& id_, const entity_lemma_t& lemma_, client_label_t& label_, entity_count_t count_):
        id(id_),
        lemma(lemma_),
        label(label_),
        count(count_)
    {}
    
    ~DocEnitiyInfo(){}
};

typedef std::vector<DocEnitiyInfo> doc_entities_t;


struct DocInfo
{
    doc_id_t id;
    doc_entities_t entities;
    
    DocInfo(){}
    DocInfo(const doc_id_t& doc_id_, const doc_entities_t& entities_):
        id(doc_id_),
        entities(entities_)
    {}

    ~DocInfo(){}
};

typedef std::vector<DocInfo> docs_info_t;


struct EntityInfo
{
    entity_id_t id;
    label_t label;
    total_entity_count_t total_count;
    total_doc_count_t total_docs_count;
    
    EntityInfo(): id(0),label(0),total_count(0),total_docs_count(0) {}
    EntityInfo(const entity_id_t& id_, 
               label_t label_, 
               total_entity_count_t total_count_, 
               total_doc_count_t total_docs_count_):
        id(id_),
        label(label_),
        total_count(total_count_),
        total_docs_count(total_docs_count_)
    {}

    ~EntityInfo(){}
};


typedef std::vector<EntityInfo> entities_t;

typedef std::pair<str_label_t,std::pair<total_entity_count_t,total_doc_count_t>> entity_labels_stat_t;
typedef std::vector<entity_labels_stat_t> entity_stat_t;

typedef unsigned long long int vault_entity_id_t;
typedef std::unordered_map<label_t,std::pair<total_entity_count_t,total_doc_count_t>> label_counters_t;
typedef std::unordered_map<str_label_t,std::pair<total_entity_count_t,total_doc_count_t>> str_label_counters_t;
typedef std::unordered_map<entity_id_t,label_counters_t> words_db_entities_t;
typedef std::unordered_map<entity_id_t,vault_entity_id_t> entity_to_vault_t;
typedef std::vector<entity_lemma_t> entities_vault_t;

typedef std::unordered_map<str_label_t,label_t> str_label_to_label_t;
typedef std::vector<str_label_t> labels_vault_t;


struct WordsDBEntityStat
{
    entity_id_t id;
    entity_lemma_t lemma;
    doc_count_t total_docs_count;
    entity_count_t total_entities_count;
    entity_stat_t stat;
    WordsDBEntityStat(){}
    ~WordsDBEntityStat(){}
};

typedef std::vector<WordsDBEntityStat> wdb_entities_stat_t;

struct WordsDBStat
{
    total_doc_count_t total_docs_count;
    total_entity_count_t total_entities_count;
    total_entity_count_t vocabulary_size;
    unsigned int  labels_size;
    std::time_t last_added_time;
    str_label_counters_t labels;
    
    WordsDBStat(total_doc_count_t total_docs_count_,
                total_entity_count_t total_entities_count_,
                total_entity_count_t vocabulary_size_,
                unsigned int labels_size_,
                std::time_t last_added_time_,
                const str_label_counters_t& labels_
                )
    {
        total_docs_count = total_docs_count_;
        total_entities_count = total_entities_count_;
        vocabulary_size = vocabulary_size_;
        labels_size = labels_size_;
        last_added_time = last_added_time_;
        labels = labels_;
    }

    ~WordsDBStat(){}
};

void to_json(nlohmann::json &j, const DocEnitiyInfo& doc_entity);

void from_json(const nlohmann::json &j, DocEnitiyInfo& doc_entity);

void to_json(nlohmann::json &j, const doc_entities_t& doc_entities);

void from_json(const nlohmann::json &j, doc_entities_t& doc_entities);

void to_json(nlohmann::json &j, const DocInfo& doc_info);

void from_json(const nlohmann::json &j, DocInfo& doc_info);

void to_json(nlohmann::json &j, const docs_info_t& docs_info);

void from_json(const nlohmann::json &j, docs_info_t& docs_info);

void to_json(nlohmann::json &j, const EntityInfo& entity);

void from_json(const nlohmann::json &j, EntityInfo& entity);

void to_json(nlohmann::json &j, const entity_stat_t& entity_info);

void from_json(const nlohmann::json &j, entity_stat_t& entity_info);

void to_json(nlohmann::json &j, const wdb_entities_stat_t& wdb_entities);

void from_json(const nlohmann::json &j, wdb_entities_stat_t& wdb_entities);

void to_json(nlohmann::json &j, const WordsDBStat& wdb_stat);

void from_json(const nlohmann::json &j, WordsDBStat& wdb_stat);
