// main application

#include <csignal>
#include <execinfo.h>

#include <functional>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <chrono>
#include <thread>

#include "jsonrpccxx/server.hpp"
#include "cpphttplibconnector.hpp"

#include "wordsdb_lib.h"
#include "wordsdb_app.h"

namespace po = boost::program_options;


WordsDBOptions parse_cli_arguments(int argc, char* argv[])
{
    po::options_description desc("Allowed options");
    std::string config_file ("etc/config/wordsdb.yml");
    desc.add_options()
        ("help,h", "print usage message")
        ("config,c", po::value(&config_file), "config file (yaml)")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    
    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        exit(0);
    }
    
    if (vm.count("config"))
    {
        config_file = vm["config"].as<std::string>();
    }

    WordsDBOptions opts (config_file);
    return opts;
}



// from vecindexercpp<-milvus
void PrintStacktrace() 
{
    std::cerr << "Call stack:" << std::endl;

    const int size = 32;
    void* array[size];
    int stack_num = backtrace(array, size);
    char** stacktrace = backtrace_symbols(array, stack_num);
    for (int i = 0; i < stack_num; ++i) {
        std::string info = stacktrace[i];
        std::cerr << info << std::endl;
    }
    free(stacktrace);
}


// https://stackoverflow.com/questions/11468414/using-auto-and-lambda-to-handle-signal
namespace {
    std::function<void(int)> _shutdown_handler;
    void signal_handler(int signal) { _shutdown_handler(signal); }
} // namespace



int main(int argc, char* argv[])
{
    try 
    {
        WordsDBOptions opts = parse_cli_arguments(argc, argv);
        WordsDBApp app (opts);
        
        jsonrpccxx::JsonRpc2Server rpcServer;
        
        add_wordsdb_app_jsonrpc_handlers(rpcServer, app);
        
        CppHttpLibServerConnector httpServer(rpcServer, opts.port);        
        
        _shutdown_handler = [&] (int signum)
        {   
            switch (signum) 
            {
                case SIGINT: 
                case SIGTERM: 
                {
                    std::cerr << "Server received signal: " << signum << std::endl;
                    std::cout << "Stoping WordsDBApp." << std::endl;
                    std::cout << "Stoping httpServer:";
                    httpServer.StopListening();
                    std::cout << "true" << std::endl;
                    app.Stop();
                    std::cout << "WordsDBApp stoped." << std::endl;
                    exit(0);
                }
                default: 
                {
                    std::cerr << "Server received critical signal: " << signum << std::endl;
                    PrintStacktrace();
                    exit(1);
                }
            }
        };
        
        /* Handle Signal */
        signal(SIGHUP, signal_handler);
        signal(SIGINT, signal_handler);
        signal(SIGUSR1, signal_handler);
        signal(SIGUSR2, signal_handler);
        signal(SIGTERM, signal_handler);
        
        std::cout << "Starting wordsdb_app on " << opts.port << ": " << std::boolalpha << httpServer.StartListening() << std::endl;
        
        pause();
        
    }
    catch(std::exception& e)
    {
        std::cerr << "WordsDBApp runtime error:" << e.what() << std::endl;
        return -1;
    }
    
    std::cerr << "WordsDBApp finished!" << std::endl;
    return 0;
}
