#pragma once

#include <mutex>
#include <condition_variable>

class Semaphore{
private:
    int                     count;
    std::mutex              locker;
    std::condition_variable cv;
public:
    explicit Semaphore():count(0) {}

    void enter(){
        std::unique_lock<std::mutex> lock(locker);
        while (count == 0) cv.wait(lock);
        --count;
    }
    void add(){
        std::lock_guard<std::mutex> lock(locker);
        ++count;
        cv.notify_one();
    }
    void add(unsigned int len){
        std::lock_guard<std::mutex> lock(locker);
        count += len;
        cv.notify_all();
    }

    void unleash(){
        count = -1;
        cv.notify_all();
    }
};
