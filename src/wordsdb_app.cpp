
#include <iostream> 

#include "wordsdb_app.h"

#include "jsonrpccxx/common.hpp"

void WordsDBApp::init()
{
    if (boost::filesystem::exists(opts.words_db_file))
    {
        std::cout << "Reading wdb from " << opts.words_db_file << std::endl;
        std::ifstream ifs(opts.words_db_file);
        boost::archive::text_iarchive oa(ifs);
        oa >> wdb;
        std::cout << "Wdb read from " << opts.words_db_file << std::endl;
        }
    else
    {
        std::cout << "Wdb file does not exist: " << opts.words_db_file << std::endl;
        wdb.init(opts);
        std::cout << "Inited empty wdb." << std::endl;
    }
}


void WordsDBApp::Stop()
{
    boost::filesystem::path wdb_file(opts.words_db_file);
    
    if (opts.words_db_lazy_writing && boost::filesystem::exists(wdb_file))
    {
        std::time_t last_added_time = wdb.getLastAddedTime();
        std::time_t last_write_time = boost::filesystem::last_write_time(wdb_file);
        if (last_added_time < last_write_time)
        {
            std::cout << "It seems like wdb was not changed. Skipping writing to a wdb file: " << wdb_file << std::endl;
            return ;
        }
    }
    
    if (wdb_file.parent_path()!="" and !boost::filesystem::exists(wdb_file.parent_path()))
    {
        boost::filesystem::create_directory(wdb_file.parent_path());
    }
    
    std::cout << "Writing wdb to " << opts.words_db_file << std::endl;
    std::ofstream ofs(opts.words_db_file);
    boost::archive::text_oarchive oa(ofs);
    oa << wdb;
    std::cout << "Wdb wrote to " << opts.words_db_file << std::endl;
}


bool WordsDBApp::AddDocumentEntities(const doc_entities_t& doc_entities)
{
    
    return wdb.addDocumentEntities(doc_entities);

        
    return true;
}


bool WordsDBApp::AddDocumentsEntities(const docs_info_t& docs_info)
{
    for (auto& doc_info: docs_info)   
        AddDocumentEntities(doc_info.entities);
    
    return true;
}


wdb_entities_stat_t WordsDBApp::AllEntities(int limit)
{
    return wdb.allEntities(limit);
}


wdb_entities_stat_t WordsDBApp::GetEntity(const entity_id_t& entity_id)
{   
    wdb_entities_stat_t entities;
    auto wdb_entity_info = wdb.getEntity(entity_id);
    entities.push_back(wdb_entity_info);
    
    return entities;
}

wdb_entities_stat_t WordsDBApp::GetEntities(const entity_ids_t& entity_ids)
{   
    wdb_entities_stat_t entities;
    for (auto& entity_id: entity_ids)
    {
        auto wdb_entity_info = wdb.getEntity(entity_id);
        entities.push_back(wdb_entity_info);
    }
    
    return entities;
}

WordsDBStat WordsDBApp::GetWdbStat(bool all)
{
    return WordsDBStat(wdb.getTotalDocsCount(), 
                       wdb.getTotalEntitesCount(), 
                       wdb.getVocabularySize(),
                       wdb.getLabelsSize(),
                       wdb.getLastAddedTime(), 
                       wdb.getStrLabelsCounters()
                       );
}

void add_wordsdb_app_jsonrpc_handlers(jsonrpccxx::JsonRpc2Server& rpcServer, WordsDBApp& app)
{
    rpcServer.Add("AddDocumentEntities", 
                    jsonrpccxx::GetHandle(&WordsDBApp::AddDocumentEntities, app), 
                    {"entities"});
    rpcServer.Add("AddDocumentsEntities", 
                    jsonrpccxx::GetHandle(&WordsDBApp::AddDocumentsEntities, app), 
                    {"docs_info"});
    rpcServer.Add("AllEntities", 
                    jsonrpccxx::GetHandle(&WordsDBApp::AllEntities, app), 
                    {"limit"});
    rpcServer.Add("GetEntity", 
                    jsonrpccxx::GetHandle(&WordsDBApp::GetEntity, app), 
                    {"id"});
    rpcServer.Add("GetEntities", 
                    jsonrpccxx::GetHandle(&WordsDBApp::GetEntities, app), 
                    {"ids"});
    rpcServer.Add("GetStat", 
                    jsonrpccxx::GetHandle(&WordsDBApp::GetWdbStat, app), 
                    {"all"});
}
